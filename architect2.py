from discord.ext import commands
from discord.ext.commands import has_permissions
from dejp import Dejp
import json
import os

# Loading config.json and passing it to dejp
with open("config/config.json") as json_file:
    config = json.load(json_file)
    prefix = config["prefix"]
    dejp = Dejp(config)
embeded_messages = {}  # Application wide dict of all embeded messages


client = commands.Bot(command_prefix=config["prefix"])  # Instance of "Bot" with the given command prefix


# Generic command to get the channel_id in Discord
@client.command()
@has_permissions(administrator=True)
async def channel_id(ctx):
    await ctx.send(ctx.channel.id)


# Reloads all embeds through dejp, overriding embeded_messages
@client.command()
@has_permissions(administrator=True)
async def reload_embeds(ctx):
    await ctx.send("DEJP is reparsing embeds json...")
    dejp.load_embeds(embeded_messages)
    await ctx.send("Work complete!")


# Loads a given extension, like calendar, through a discord command (filename without .py)
@client.command()
@has_permissions(administrator=True)
async def load(ctx, extension):
    client.load_extension(f"cogs.{extension}")
    await ctx.send(f"{extension} loaded successfully.")


# Unloads a given extension, like calendar, through a discord command (filename without .py)
@client.command()
@has_permissions(administrator=True)
async def unload(ctx, extension):
    client.unload_extension(f"cogs.{extension}")
    await ctx.send(f"{extension} unloaded successfully.")


# Loads all extensions at first startup
for filename in os.listdir("cogs"):
    if filename.endswith(".py"):
        client.load_extension(f"cogs.{filename[:-3]}")


# First init of all embeds and start of client
dejp.load_embeds(embeded_messages)
client.run(config["token"])
