architect2 PyCharm Project.

Architect2 is the second generation of Architect, a Discord bot to maintain 
and support Discord communities.

It is a modular system that has a sleek core which is expanded by extensions.

Current extensions:
    -Shortterm
    -Assistant
    -Calendar
    
    
Create Config:
    -create config folder
    -create config.json in config folder
    -Change in architect2.py -> client.run(config["token"])
    -Add following:
    
    
{
    "prefix": "!",
    "token": "Your token",
    "vc_sorting": "Short Term",
    "cat_name": "Temporaer",
    "st_vc_name": "ST Voice: {}",
    "st_tc_name": "ST Text: {}",
    "embed_dir": "embeds"
}