from discord.ext import commands
from architect2 import config
import discord
import time


class Shortterm(commands.Cog):
    tmp_vc = []  # Temporary Voice Channel list
    tmp_tc = {}  # Temporary Text Channel dict to connect tc to associated vc

    def __init__(self, client):
        self.client = client

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        """
        -Checks if the channel the user joined next is not none, that would imply that he left
         and if the current Voice Channel is the sorting channel.
        -Gets the category under which the channels should be created
        -Creates the Voice Channel with: name, category and user limit
        -Creates the Text Channel with: name, category
        -Adds the Voice Channel to the temporary VC list
        -Adds the Voice Channel, as key, and Text Channel, as value, to the temporary TC dict
        -Moves the member to the newly created Voice Channel"""
        if after.channel is not None and str(member.voice.channel.name) == config["vc_sorting"]:
            category = discord.utils.get(member.guild.categories, name=config["cat_name"])
            vc = await member.guild.create_voice_channel(config["st_vc_name"].format(member.name), category=category,
                                                         user_limit=10)
            tc = await member.guild.create_text_channel(config["st_tc_name"].format(member.name), category=category)
            self.tmp_vc.append(vc)
            self.tmp_tc[vc] = tc
            await member.move_to(vc)

        time.sleep(3)  # Waits 3 seconds to iron out short disconnects

        """
        -Goes through all Channels
        -Checks if the Channel is under the temporary category, Checks if the Channel ist from type voice
         and Checks if the Channel is empty
        -Pops the Text Channel from the tmp_tc dict through the given vc
        -Removes the Voice Channel from the tmp_vc list
        -Deletes both, vc and tc"""
        for vc in self.client.get_all_channels():
            if str(vc.category) == config["cat_name"] and str(vc.type) == "voice" and len(vc.members) == 0:
                tc = self.tmp_tc.pop(vc)
                self.tmp_vc.remove(vc)
                await vc.delete()
                await tc.delete()


# Setup function to create and add the extension to client
def setup(client):
    client.add_cog(Shortterm(client))
