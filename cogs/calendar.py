from discord.ext import commands, tasks
from discord.ext.commands import has_permissions
from datetime import date, datetime, timedelta
import discord
import csv


# A single Event from the calendar, with all information's we need
class Lecture:
    ping_count = 2

    def __init__(self, summary, start_date, start_time):
        self.summary = summary
        self.start_date = start_date
        self.start_time = start_time


# Creates list of lectures, reads the csv (outlook export), create a new lecture for each row.
lectures = []
with open("wiss20.CSV", encoding="utf-8", errors="replace") as csv_file:
    csv_reader = csv.reader(csv_file)
    for row in csv_reader:
        lectures.append(Lecture(row[0], row[1], row[2]))


# Calculates the difference between two times in minutes
def time_diff(time1, time2):
    time_a = datetime.strptime(time1, "%H:%M:%S")
    time_b = datetime.strptime(time2, "%H:%M:%S")
    diff = time_a - time_b
    return diff.seconds / 60


class Calendar(commands.Cog):
    channel = 0  # Notification channel, set through notification start

    def __init__(self, client):
        self.client = client

    # Sets the notification channel, lets the user know, ands starts notification loop
    @commands.command()
    @has_permissions(administrator=True)
    async def start_notification(self, ctx):
        self.channel = ctx.channel
        await ctx.send(f"Starting notifications in {self.channel}")
        self.notification_loop.start()

    # Stops the notification loops, lets the user know
    @commands.command()
    @has_permissions(administrator=True)
    async def stop_notification(self, ctx):
        await ctx.send("Stopping notifications...")
        self.notification_loop.stop()

    # Sends the lectures of a given day, as dm: !lectures [arg1], arg1 = time delta
    @commands.command()
    async def lectures(self, ctx, arg1=0):
        dmc = await ctx.author.create_dm()  # Direct Message channel
        now = datetime.today() + timedelta(days=arg1)  # Add timedelta from command
        now = now.strftime("%-d.%-m.%Y")  # Sets date to linux format
        for e in lectures:
            if e.start_date == now:  # Checks start date
                embed = discord.Embed(
                    title=e.summary,
                    description=f"Beginnt um {e.start_time}",
                    color=discord.Color.dark_blue()
                )
                await dmc.send(embed=embed)

    # Checks every 5 minutes if there's a lecture starting today in the next 15 minutes
    @tasks.loop(minutes=5.0)
    async def notification_loop(self):
        today = date.today().strftime("%-d.%-m.%Y")  # Today date in linux format
        now = datetime.now().time().strftime("%H:%M:%S")  # Current time
        for e in lectures:
            # Checks if the lecture date ist today and if it start within 15 minutes
            if e.start_date == today and time_diff(e.start_time, now) <= 15:
                e.ping_count -= 1  # To only post it twice
                embed = discord.Embed(
                    title=e.summary,
                    description=f"Beginnt um {e.start_time}",
                    color=discord.Color.dark_blue()
                )
                embed.set_author(name=f"IN {time_diff(e.start_time, now):0.5} MINUTEN STARTET:")
                embed.set_footer(text=f"Gesendet um: {now}")
                await self.channel.send(embed=embed)
                if e.ping_count <= 0:
                    lectures.remove(e)  # Removes lecture if it was send twice


# Setup function to create and add the extension to client
def setup(client):
    client.add_cog(Calendar(client))
