from discord.ext import commands
from dejp import parse
from architect2 import embeded_messages
import discord


# Useless filler extension, until I find something useful to do here.
class Assistant(commands.Cog):
    def __init__(self, client):
        self.client = client

    # Sets the activity of the bot.
    @commands.Cog.listener()
    async def on_ready(self):
        await self.client.change_presence(activity=discord.Activity(name="!a2 Info", type=discord.ActivityType.playing))

    # Posts the a2 embed
    @commands.command(aliases=["a", ""])
    async def a2(self, ctx):
        await ctx.send(embed=parse(embeded_messages["a2"]))


# Setup function to create and add the extension to client
def setup(client):
    client.add_cog(Assistant(client))
