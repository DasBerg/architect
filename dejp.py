import discord
import json
import os


# TODO: Extend parser to all Discord.Embed functions
def parse(json_parse):
    embed = discord.Embed(
        title=json_parse["title"],
        description=json_parse["description"],
        color=discord.Color.dark_blue()
    )
    embed.set_author(name=json_parse["author"]["name"])
    embed.set_footer(text=json_parse["footer"]["text"])
    for f in json_parse["fields"]:
        embed.add_field(name=f["name"],
                        value=f["value"],
                        inline=f["inline"])
    return embed


# Discord Embeds Json Parser
class Dejp:
    def __init__(self, config):
        self.config = config

    """Loads all .json from embeds folder and stores the parsed json in a dictionary,
    using the filename without .json as key and the parse as value"""
    def load_embeds(self, em):
        for file in os.listdir(self.config["embed_dir"]):
            if file.endswith(".json"):
                key = file[:-5]
                with open(str(self.config["embed_dir"] + "/" + file)) as parse_file:
                    value = json.load(parse_file)
                    em[key] = value
